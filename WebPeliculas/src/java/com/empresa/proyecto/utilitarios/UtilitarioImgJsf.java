/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.utilitarios;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

/**
 *
 * @author yio
 */
public class UtilitarioImgJsf {
    
    
    public static String guardaBlobEnFicheroTemporal(byte[] bytes, String nombreArchivo){
        String ubicacionImagen = null;
         ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
         //apunta a la ruta del servidor
         String path= servletContext.getRealPath("") + File.separatorChar + "resources" + File.separatorChar 
                  + "img" + File.separatorChar + "tmp" + File.separatorChar + nombreArchivo;
          
                 File f;
                 InputStream in; 
            try {
                   f = new File(path);
                   in = new ByteArrayInputStream(bytes);
                  FileOutputStream out = new FileOutputStream(f.getAbsolutePath());

                 int c;
                 while((c = in.read()) >=0){
                    out.write(c);
                  }
                  out.flush();
                  out.close();
                   ubicacionImagen = "../../resources/img/tmp/" + nombreArchivo;
               } catch (Exception e) {
                System.err.println("No se pudo cargar la imagen");
            }
            return ubicacionImagen;
    }   
}
 
