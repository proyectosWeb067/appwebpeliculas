/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.impl;

import HibernateUtil.HibernateUtil;
import com.empresa.proyecto.dao.SerieInterface;
import com.empresa.proyecto.model.Serie;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author yio
 */
public class SerieDaoImpl implements SerieInterface{

    @Override
    public List<Serie> getSeries() {
        List<Serie> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        Transaction t = session.beginTransaction();
        String sql =  "FROM Serie";
        try {
           lista = session.createQuery(sql).list();
               t.commit();
               session.close();
        } catch (Exception e) {
           t.rollback();
        } 
        return lista;
    }

    @Override
    public void addSerie(Serie s) {
        Session session= null;
        
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(s);
            session.getTransaction().commit();
             
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
    }

    @Override
    public void deleteSerie(List<Serie> ids) {
         Session session = null;
         
        try {
          session = HibernateUtil.getSessionFactory().openSession();
            String sql = "delete Serie p where p.idSerie = :id";
            Query query = session.createQuery(sql);
            for(Serie serie : ids){
                query.setInteger("id",serie.getIdSerie());
                 query.executeUpdate();
 
             } 
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            session.getTransaction().rollback();
        }finally {
            if (session != null) {
                session.close();
            }
        }
        
    }

    @Override
    public void updateSerie(Serie s) {
         Session session= null; 
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(s);
            session.getTransaction().commit(); 
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        

    }
    
    @Override
    public Serie getSerie(Integer key) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Serie s = new Serie();
        try {
            String sql = "FROM Serie WHERE idSerie = :key";
            Query q = session.createQuery(sql);
            q.setParameter("key", key);
            s = (Serie)q.uniqueResult();
            
        } catch (Exception e) {
            session.getTransaction().rollback();
        }finally{
            session.close();
        }
        return s;
    }
    
}
