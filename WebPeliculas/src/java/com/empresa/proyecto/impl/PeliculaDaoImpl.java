/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.impl;

import HibernateUtil.HibernateUtil;
import com.empresa.proyecto.dao.PeliculaInterface;
import com.empresa.proyecto.model.Director;
import com.empresa.proyecto.model.Pelicula;
import java.util.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author yio
 */
public class PeliculaDaoImpl implements PeliculaInterface{

    @Override
    public List<Director> getDirector(){
        List<Director> listaDir  = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        String sql = "FROM Director";
        try {
               listaDir = session.createQuery(sql).list();
               t.commit();
               session.close();
        } catch (Exception e) {
            t.rollback();
        }
        
        return listaDir;
    }
    
    @Override
    public List<Pelicula> getPeliculas() {
        List<Pelicula> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession(); 
         Transaction t = session.beginTransaction();
          String sql = "FROM Pelicula";
          try {
               lista = session.createQuery(sql).list();
               t.commit();
               session.close();
        } catch (Exception e) {
            t.rollback();
        }
          return lista;
    }

    @Override
    public String addPelicula(Pelicula p) {
        Session session= null;
        String msg = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(p);
            session.getTransaction().commit();
            msg ="Se Agrego la pelicula de manera exitosa";
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            msg ="No se pudo guardar";
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return msg;
    }

    @Override
    public String deletePelicula(List<Pelicula> ids) {
        Session session = null;
        String msg = null; 
        try {
          session = HibernateUtil.getSessionFactory().openSession();
            String sql = "delete Pelicula p where p.idPelicula = :id";
            Query query = session.createQuery(sql);
            for(Pelicula p : ids){
                query.setInteger("id",p.getIdPelicula());
                 int resultado = query.executeUpdate();

                if (resultado == 0) {
                    msg = "Error, no se eliminaron los datos";
                }
             } 
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            session.getTransaction().rollback();
        }finally {
            if (session != null) {
                session.close();
            }
        }
        return msg;
    }

    @Override
    public String updatePelicula(Pelicula p) {
        Session session= null;
        String msg = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(p);
            session.getTransaction().commit();
            msg ="Se Actualizo de manera exitosa";
        } catch (Exception e) { 
            msg = "No se pudo realizar la modificación";
            System.err.println(e.getMessage()); 
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return msg;
    }

    @Override
    public Pelicula getPelicula(Integer key) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Pelicula p = new Pelicula();
        try {
            String hql = "FROM Pelicula WHERE idPelicula = :key";
            Query q = session.createQuery(hql);
            q.setParameter("key", key);
            p = (Pelicula) q.uniqueResult();
        } catch (Exception e) {
            session.getTransaction().rollback();
        }finally{
            session.close();
        } 
        return p;
    }

    //tomo en este caso solo como EJEMPLO funcional la calificacion de 4 y 5 
    @Override
    public List<Pelicula> getPeliMasVistas() {
        List<Pelicula> listap = getPeliculas();
         
        List<Pelicula> pVistas = new ArrayList();
        for(Pelicula p: listap){
              
        if(p.getCalificacion() == 4 || p.getCalificacion() == 5){
             pVistas.add(p);
           }
        }    
        return pVistas;
    }
 
    //peliculas mas valoradas(probando stream)
    @Override
    public List<Pelicula> getMasValorados(){
        List<Pelicula> lstp = getPeliculas();
        List<Pelicula> pMasVistas = new ArrayList();
         
        lstp.stream().filter((p) -> (p.getCalificacion() == 5)).forEach((p) -> {
            pMasVistas.add(p);
        });
        return pMasVistas;
    }

}
