/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.bean;

import com.empresa.proyecto.dao.SerieInterface;
import com.empresa.proyecto.impl.SerieDaoImpl;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.empresa.proyecto.model.Serie;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.UploadedFile;
/**
 *
 * @author yio
 */
@ManagedBean
@ViewScoped
public class SerieBean {
    
    private List<Serie> listas;
    private Serie serie;
    
    
    private List<Date> multi;
    private List<Date> range;
    private List<Date> invalidDates;
    private List<Integer> invalidDays;
    private Date minDate;
    private Date maxDate;
    
    //seleccion de table
    private List<Serie> selecionSerieTabla;
    
     //Subir img a bd 
     private UploadedFile uploadedFile;
     
    public SerieBean(){
        
    }
     //estableciendo el calendario para cuando quiera editar una pelicula
    //---------------------------------------------------
        @PostConstruct
    public void init() {
        invalidDates = new ArrayList<>();
        Date today = new Date();
        invalidDates.add(today);
        long oneDay = 24 * 60 * 60 * 1000;
        for (int i = 0; i < 5; i++) {
            invalidDates.add(new Date(invalidDates.get(i).getTime() + oneDay));
        }

        invalidDays = new ArrayList<>();
        invalidDays.add(0);
        /* the first day of week is disabled */
        invalidDays.add(3);

        minDate = new Date(today.getTime() - (365 * oneDay));
        maxDate = new Date(today.getTime() + (365 * oneDay));
    }
    
    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }

    public void click() {
        //tube que agregar el jar de primefaces 6 para pode imortar la libreria Prmefaces.current()
        PrimeFaces.current().ajax().update("form:display");
        PrimeFaces.current().executeScript("PF('dlg').show()");

    }
    
    public List<Date> getMulti() {
        return multi;
    }

    public void setMulti(List<Date> multi) {
        this.multi = multi;
    }

    public List<Date> getRange() {
        return range;
    }

    public void setRange(List<Date> range) {
        this.range = range;
    }

    public List<Date> getInvalidDates() {
        return invalidDates;
    }

    public void setInvalidDates(List<Date> invalidDates) {
        this.invalidDates = invalidDates;
    }

    public List<Integer> getInvalidDays() {
        return invalidDays;
    }

    public void setInvalidDays(List<Integer> invalidDays) {
        this.invalidDays = invalidDays;
    }

    public Date getMinDate() {
        return minDate;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }
    //-------------------------------------------------------------------------------
    

    //---------metodos get and set para subir img a bd---------
      public UploadedFile getUploadedFile() {
        return uploadedFile;
    }
    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    //----------------------------------------------------------
    
    //-----------metodos set y get para seleccion de ids de tabla--
    public List<Serie> getSelecionSerieTabla() {
        return selecionSerieTabla;
    }

    public void setSelecionSerieTabla(List<Serie> selecionSerieTabla) {
        this.selecionSerieTabla = selecionSerieTabla;
    }
    
     
    //-------------------------------------------------------------
 
    // Metodo que me retorna la lista de peliculas almacenas
    public List<Serie> getListas() {
        SerieInterface si = new SerieDaoImpl();
        listas = si.getSeries(); 
        return listas;
    }

    public void setListas(List<Serie> listas) {
        this.listas = listas;
    }

    public Serie getSerie() {
        return serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }
    
     //Metodo preparar nueva serie
    public void prepararNuevaSerie()
    {
        serie = new Serie();
    }
    //metodo que me agrega una Serie
    public void addPelicula(){
       
       
        try {
                SerieInterface serieInterface = new SerieDaoImpl();
                InputStream input = uploadedFile.getInputstream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                
                //arranca en cero y va hasta la lectura del buffer si es mayor a cero
                for(int length=0;(length=input.read(buffer))>0;){
                    output.write(buffer, 0, length);
                }      
                serie.setImagen(output.toByteArray()); 
                serieInterface.addSerie(serie);
        } catch (Exception e) {
        }
        
        
    }
    //metodo que me permite actualizar una serie
    public void updateSerie(){
        
        try {
                SerieInterface interfaceSe = new SerieDaoImpl();
                InputStream input = uploadedFile.getInputstream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                
                //arranca en cero y va hasta la lectura del buffer si es mayor a cero
                for(int length=0;(length=input.read(buffer))>0;){
                    output.write(buffer, 0, length);
                }      
                serie.setImagen(output.toByteArray());
                interfaceSe.updateSerie(serie);
                serie = new Serie(); //limpiando el objeto
        } catch (Exception e) {
        } 
         
    } 
    //Metodo que me permite eliminar una o varias serie
    public void deleteSerie(){
            
       SerieInterface peliculaInterface = new SerieDaoImpl();
        peliculaInterface.deleteSerie(selecionSerieTabla);
        
       
    }
}
