/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.bean;

import com.empresa.proyecto.dao.PeliculaInterface;
import com.empresa.proyecto.impl.PeliculaDaoImpl;
import com.empresa.proyecto.model.Director;
import com.empresa.proyecto.model.Pelicula;
import com.empresa.proyecto.utilitarios.UtilitarioImgJsf;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date; 
import java.util.List; 
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author yio
 */
@ManagedBean
@ViewScoped
public class PeliculaBean {
    private List<Pelicula> listap;
    private Pelicula pelicula;
    
    private Date date;
    private List<Date> multi;
    private List<Date> range;
    private List<Date> invalidDates;
    private List<Integer> invalidDays;
    private Date minDate;
    private Date maxDate;
    //para obtener la imagen
    private String imagenPelicula;
    
    //seleccion de tabla
    private List<Pelicula> selecionPeliculaTabla;
    
    //lista directores
     private List<Director> listaDir;
     
   //lista peliculas mas  vistas
     private List<Pelicula> lstpelVistas;
     
   //lista peliculas mas valoradas
     private List<Pelicula> lstpelMasValoradas;
    
  //Subir img a bd 
     private UploadedFile uploadedFile;
    
    public PeliculaBean() {
         
    }
    
    //estableciendo el calendario para cuando quiera editar una pelicula
    //---------------------------------------------------
        @PostConstruct
    public void init() {
        invalidDates = new ArrayList<>();
        Date today = new Date();
        invalidDates.add(today);
        long oneDay = 24 * 60 * 60 * 1000;
        for (int i = 0; i < 5; i++) {
            invalidDates.add(new Date(invalidDates.get(i).getTime() + oneDay));
        }

        invalidDays = new ArrayList<>();
        invalidDays.add(0);
        /* the first day of week is disabled */
        invalidDays.add(3);

        minDate = new Date(today.getTime() - (365 * oneDay));
        maxDate = new Date(today.getTime() + (365 * oneDay));
        
        
       
    }
    
    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }

    public void click() {
        //tube que agregar el jar de primefaces 6 para pode imortar la libreria Prmefaces.current()
        PrimeFaces.current().ajax().update("form:display");
        PrimeFaces.current().executeScript("PF('dlg').show()");

    }
     public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Date> getMulti() {
        return multi;
    }

    public void setMulti(List<Date> multi) {
        this.multi = multi;
    }

    public List<Date> getRange() {
        return range;
    }

    public void setRange(List<Date> range) {
        this.range = range;
    }

    public List<Date> getInvalidDates() {
        return invalidDates;
    }

    public void setInvalidDates(List<Date> invalidDates) {
        this.invalidDates = invalidDates;
    }

    public List<Integer> getInvalidDays() {
        return invalidDays;
    }

    public void setInvalidDays(List<Integer> invalidDays) {
        this.invalidDays = invalidDays;
    }

    public Date getMinDate() {
        return minDate;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }
    
   
    //-----metodos set y get para seleccion de ids de tabla------
        public List<Pelicula> getSelecionPeliculaTabla() {
        return selecionPeliculaTabla;
    }

    public void setSelecionPeliculaTabla(List<Pelicula> selecionPeliculaTabla) {
        this.selecionPeliculaTabla = selecionPeliculaTabla;
    } 
    //----------------------------------------------------------- 
    
    //--------get and set para subir imagen a la bd--------------------------------
     public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    } 
   //---------------------------------------------------------------------
    
    
    //-----------Metodos seter and geter para directores Pendiente para posible utilizacion-----
    //Metodo que me optiene una lista de directores
    public List<Director> getListaDir() {
       PeliculaInterface pi = new PeliculaDaoImpl();
          listaDir = pi.getDirector();
        return listaDir;
    }

    public void setListaDir(List<Director> listaDir) {
        this.listaDir = listaDir;
    } 
    //---------------------------------------------------------------------
    
  
   //--- -------metodos set ans get de peliculas mas vistas--------------
    
    public List<Pelicula> getLstpelVistas() {
         PeliculaInterface peliculaInterface = new PeliculaDaoImpl();
           lstpelVistas= peliculaInterface.getPeliMasVistas();
         
       return lstpelVistas;
    } 
    public void setLstpelVistas(List<Pelicula> lstpelVistas) {     
        this.lstpelVistas = lstpelVistas;
    }

    //--------------------------------------------------------------------
    //------metodo que optiene la lista de peliculas-----------------------
    public List<Pelicula> getListap() {
        PeliculaInterface peliculaInter = new PeliculaDaoImpl();
        listap = peliculaInter.getPeliculas();
        return listap;
    }

    public void setListap(List<Pelicula> listap) {
        this.listap = listap;
    }
   //-------------------------------------------------------------------------
    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }
    //almacenara la ruta donde se encuentra la imagen de la pelicula
    public String getImagenPelicula() {
        return imagenPelicula;
    }

    public void setImagenPelicula(String imagenPelicula) {
        this.imagenPelicula = imagenPelicula;
    }
     
    //Metodo que me permite subir la imagen PRUEBA
    public void subirImagen(FileUploadEvent event){
       FacesMessage message = new FacesMessage();
        try {
             pelicula.setImagen(event.getFile().getContents());
            imagenPelicula = UtilitarioImgJsf.guardaBlobEnFicheroTemporal(pelicula.getImagen(),event.getFile().getFileName());
            message.setSeverity(FacesMessage.SEVERITY_INFO);
            message.setSummary("Registro cargado correctamente");
        } catch (Exception e) {
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Problemas al subir la imagen");
        }
        FacesContext.getCurrentInstance().addMessage("Mensaje", message);
    } 
    //Metodo preparar nueva pelicula
    public void prepararNuevaPelicula()
    {
        pelicula = new Pelicula();
    }
    
    
    //metodo que me permite agregar una pelicula
    
    public String addPelicula(){
        String msg = null;
        
        //----------agrego nuevo codigo para guardar img en bd----------
        try {   PeliculaInterface peliculaInterface = new PeliculaDaoImpl();
                InputStream input = uploadedFile.getInputstream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                
                //arranca en cero y va hasta la lectura del buffer si es mayor a cero
                for(int length=0;(length=input.read(buffer))>0;){
                    output.write(buffer, 0, length);
                }      
                pelicula.setImagen(output.toByteArray()); 
                  //devuelve el mensaje validando una vez agregada la pelicula
                msg = peliculaInterface.addPelicula(pelicula);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Correcto", msg)); 
               
        } catch (Exception e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error", e.getMessage()));
        }
        //---------------------------------------------------------------
        
         
       
         return msg; 
    }
    
    //metodo que me permite actualizar una pelicula
    public String updatePelicula(){
        String msg = null;
       
        //----------agrego nuevo codigo para guardar img en bd----------
        try {  
                PeliculaInterface peliculaInterface = new PeliculaDaoImpl();
                InputStream input = uploadedFile.getInputstream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                
                //arranca en cero y va hasta la lectura del buffer si es mayor a cero
                for(int length=0;(length=input.read(buffer))>0;){
                    output.write(buffer, 0, length);
                }      
                pelicula.setImagen(output.toByteArray()); 
               
                msg = peliculaInterface.updatePelicula(pelicula);
                  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Correcto", msg));
                pelicula = new Pelicula(); //limpiando el objeto
        } catch (Exception e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error", e.getMessage()));
        }
        //---------------------------------------------------------------
        
       
        
        return msg;
    } 
    
    //Metodo que me elimina la eleccion realizada en la vista
    public String deletePelicula(){
         
      String msg = null;  
       PeliculaInterface peliculaInterface = new PeliculaDaoImpl();
       msg = peliculaInterface.deletePelicula(selecionPeliculaTabla);
        
        /*for(Pelicula p: selecionPeliculaTabla){
             System.out.println("ides enviados--->" + p.getIdPelicula());
            ids = Integer.toString(p.getIdPelicula()); 
        } 
        String arrayIds[] = ids.split(",");
        
        for(int i= 0; i< arrayIds.length; i++){
              System.out.println("numeros string --> " + arrayIds[i]);
            } */ 
        return msg;
    } 
   
     //Metodo que me entrega las peliculas mas valoradas

    public List<Pelicula> getLstpelMasValoradas() {
        PeliculaInterface pi = new PeliculaDaoImpl();
         lstpelMasValoradas = pi.getMasValorados();
        
        return lstpelMasValoradas;
    }

    public void setLstpelMasValoradas(List<Pelicula> lstpelMasValoradas) {
        this.lstpelMasValoradas = lstpelMasValoradas;
    }
     
}
