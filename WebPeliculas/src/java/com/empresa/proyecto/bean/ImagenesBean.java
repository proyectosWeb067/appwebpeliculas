/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.bean;

import com.empresa.proyecto.dao.PeliculaInterface;
import com.empresa.proyecto.impl.PeliculaDaoImpl;
import com.empresa.proyecto.model.Pelicula;
import java.io.IOException;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import java.io.ByteArrayInputStream;

/**
 *
 * @author yio
 */
@ManagedBean
@ApplicationScoped
public class ImagenesBean {
    
    public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            String idpeli = context.getExternalContext().getRequestParameterMap().get("idpeli");
            PeliculaInterface pi = new PeliculaDaoImpl();
            Pelicula pelicula = pi.getPelicula(Integer.valueOf(idpeli));
            return new DefaultStreamedContent(new ByteArrayInputStream(pelicula.getImagen()));
        }
    }
    
}
