/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.dao;
import com.empresa.proyecto.model.Serie;
import java.util.List;
/**
 *
 * @author yio
 */
public interface SerieInterface {
    //metodo que obtiene todas las serie
    public List<Serie> getSeries();
    
    //metodo que agrega una serie
    public void addSerie(Serie s);
    
    //metodo que elimina una serie
    public void deleteSerie(List<Serie> ids);
    
    //metodo que actualiza una serie
    public void updateSerie(Serie s);
    
    //metodo que me obtine una serie
    public Serie getSerie(Integer key);
}
