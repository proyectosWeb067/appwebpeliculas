 
package com.empresa.proyecto.dao;
import com.empresa.proyecto.model.Director;
import com.empresa.proyecto.model.Pelicula;
import java.util.List;

 
public interface PeliculaInterface {
    
    //metodo que obtiene todas las peliculas
    public List<Pelicula> getPeliculas();
    
    //metodo que me obtine todos los directores
    public List<Director> getDirector();
    
    
    //metodo que agrega una pelicula
    public String addPelicula(Pelicula p);
    
    //metodo que elimina una pelicula
    public String deletePelicula(List<Pelicula> ids);
    
    //metodo que actualiza una pelicula
    public String updatePelicula(Pelicula p);
    
    //metodo que me obtine una pelicula
    public Pelicula getPelicula(Integer key);
    
    //metodo que me optiene las peliculas mas vistas
    public List<Pelicula> getPeliMasVistas();
    
    //metodo para obtener las mas valoradas
    public List<Pelicula> getMasValorados();
}
