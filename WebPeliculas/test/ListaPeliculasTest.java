/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.empresa.proyecto.dao.PeliculaInterface;
import com.empresa.proyecto.impl.PeliculaDaoImpl;
import com.empresa.proyecto.model.Pelicula;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author juan
 */
public class ListaPeliculasTest {
    
    public ListaPeliculasTest() {
    }
    
    @Before
    public void setUp() {
        System.out.println("Ha iniciado el test..");
    }
    
     @Test
    public void listaTest(){
        PeliculaInterface pi = new PeliculaDaoImpl();
        List<Pelicula> lista = pi.getPeliculas();
        
       assertNotNull("no esta nulo", lista);
        
    }
    
    @After
    public void tearDown() {
        System.out.println("El test ha finalizado..");
    }
    
   
}
